module weavelab.xyz/graphroto

go 1.14

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/vektah/gqlparser/v2 v2.1.0
)
