package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"weavelab.xyz/graphroto/graph/generated"
	"weavelab.xyz/graphroto/graph/model"
)

func (r *mutationResolver) PaymentsMakePayment(ctx context.Context, in *model.MakePaymentInput) (*model.Payment, error) {
	if in == nil {
		return nil, fmt.Errorf("so sorry...we need some input")
	}
	locationID := "myweavelocation"
	moar := "hello proto -> graphql"
	return &model.Payment{
		LocationID:  &locationID,
		ProcessorID: in.ProcessorID,
		Amount:      in.Amount,
		MoarData:    &moar,
	}, nil
}

func (r *queryResolver) Dummy(ctx context.Context) (*bool, error) {
	panic(fmt.Errorf("not implemented"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
