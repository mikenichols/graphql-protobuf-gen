# graphroto

## Prereqs

- `protoc` v3+ https://github.com/protocolbuffers/protobuf/releases/tag/v3.14.0
- be sure to copy the `include` folder included in the package (for google proto defs)
- be sure `protoc` is on your $PATH
- install proto plugins:
    - `go install github.com/danielvladco/go-proto-gql/protoc-gen-gql`
    - `go install github.com/danielvladco/go-proto-gql/protoc-gen-gogql`

## Structure

- `pb` directory holds protobuf definitions.
- `graph` is output for `gqlgen` generated stuff

`graph/*.resolvers.go` are the place where you implement the resolvers for mutations or queries. 
it's easy to imagine domain services exporting go packages that have adapters for wiring directly to the rpc implementations (if 1:1 mapping is really wanted).

## Generating Things

1. `make`
2. `go run server.go`

Visit http://localhost:8080

Example query:
```
mutation MakePayment($in: MakePaymentInput) {
  paymentsMakePayment_(in: $in) {
    locationId
    amount
  }
}
```

Use "Query Variables", eg:
```
{
  "in": {
    "processorId": "123",
    "amount":42
  }
}
```
